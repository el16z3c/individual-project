#include "Navigator.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int frontNum;
Node ** frontList;
int searchNum;
Node ** searchList;

Node ** Navigator( Node * startNode, Node ** nodeList)
{
  //copy the nodelist in order to search from
  searchNum = nodeNum;
  searchList = (Node **)malloc(searchNum * sizeof( Node * ));
  int i;
  for( i = 0; i < searchNum ; i++)
  {
    searchList[i]=nodeList[i];
  }

  frontList = malloc(sizeof(Node *));
  frontList[0] = startNode;
  frontNum = 1;

  // initialDistance();//allocate a initial value of Dist to all nodes
  initialTmpShortest();
  initialSearchIndex();
  initialMark();//the node not on the path has the default mark 0
  initialSearched();

  startNode->pathMark = 1;

  Node * origin = startNode;
  int epoch = 1;

  while (epoch <1500)
  {
    origin = FindOptimalInFrontList(origin);
    origin = moveOrigin(origin);
    initialTmpShortest();
    localSearch_2(origin, NULL, 0);
    formFrontier();
    printEpoch( nodeList , epoch );
    printf("Epoch %d generated successfully, please check it in the ./map/outfile folder.\n", epoch);
    if (frontNum == 0)
    {
      printf("Finished %d epoches!\n", epoch); //when run out of search list
      return nodeList;
    }
    epoch++;
  }
  printf("Halted at 1500 epoches.\n");
  return nodeList;
}


//flush and shortest distance to temp origin idea
void localSearch(Node * node, Node * prev, float tmpDist){
  float extendedDist;
  node->pathMark = 1;
  if (tmpDist < node->tmpShortest){
    node->tmpShortest = tmpDist;
  }
  if (tmpDist < node->searchIndex){
    node->searchIndex = tmpDist;
  }
  for(int i = 0 ; i < node->neighborNum ; i++)
  {
    extendedDist = tmpDist + node->neighborDist[i];
    if (node->neighbor[i] != prev && extendedDist <= 50 && extendedDist < node->neighbor[i]->searchIndex){
      localSearch(node->neighbor[i], node, extendedDist);
    }
  }
}

void localSearch_2(Node * node, Node * prev, float tmpDist){
  float extendedDist;
  node->searched = 1;
  node->pathMark = 1;
  for(int i = 0 ; i < node->neighborNum ; i++)
  {
    extendedDist = tmpDist + node->neighborDist[i];
    if (extendedDist < node->neighbor[i]->tmpShortest){
      node->neighbor[i]->tmpShortest = extendedDist;
    }
    if (extendedDist < node->neighbor[i]->searchIndex){
      node->neighbor[i]->searchIndex = extendedDist;
    }
  }
  for(int i = 0 ; i < node->neighborNum ; i++)
  {
    extendedDist = tmpDist + node->neighborDist[i];
    if (extendedDist < node->neighbor[i]->searchIndex || (extendedDist == node->neighbor[i]->searchIndex && node->neighbor[i]->searched != 1)){
      if (node->neighbor[i] != prev && extendedDist <= 50){
        localSearch_2(node->neighbor[i], node, extendedDist);
      }
    }
  }
}



Node * moveOrigin(Node * node){
  int maxDist = 0;
  Node * new;
  for(int i = 0 ; i < node->neighborNum ; i++)
  {
    if (node->neighbor[i]->pathMark != 1 && node->neighborDist[i] > maxDist){
      maxDist = node->neighborDist[i];
      new = node->neighbor[i];
    }
  }
  return new;
}

void formFrontier(){
  frontNum = 0;
  frontList = realloc(frontList,sizeof(Node*));
  frontList[0] = NULL;
  int i;
  for( i = 0; i < searchNum ; i++)
  {
    getFrontier(searchList[i]);
  }
}

void getFrontier(Node * node) //for every neighbor of the given node, revise its Dist and check if it is the end node
{
  int markCount = 0;
  for(int i = 0 ; i < node->neighborNum ; i++)
  {
    markCount = markCount + node->neighbor[i]->pathMark;
  }
  if ( markCount != 0 && markCount != node->neighborNum )
  {
    frontNum++;
    frontList = realloc(frontList,frontNum*sizeof(Node*));
    frontList[frontNum-1] = (Node *)malloc( sizeof( Node ) );
    frontList[frontNum-1] = node;
  }
}

Node * FindOptimalInFrontList(Node * node) // find the node with the smallest distance in search list
{
  int i;
  Node * Opti = frontList[0];
  // Opti->Dist = (Opti->lat - node->lat) * (Opti->lat - node->lat) + (Opti->lon - node->lon) * (Opti->lon - node->lon);
  for( i = 0 ; i < frontNum ; i++ )
  {
    // frontList[i]->Dist = (frontList[i]->lat - node->lat) * (frontList[i]->lat - node->lat) + (frontList[i]->lon - node->lon) * (frontList[i]->lon - node->lon);
    if (frontList[i]->tmpShortest < Opti->tmpShortest)
    {
      Opti = frontList[i];
    }
  }
  return Opti;
}

void initialTmpShortest(){
  int i;
  for ( i = 0; i < searchNum ; i++)
  {
    searchList[i]->tmpShortest = 9999999; // a huge number that larger than any distance between two points
  }
}

void initialSearchIndex(){
  int i;
  for ( i = 0; i < searchNum ; i++)
  {
    searchList[i]->searchIndex = 9999999; // a huge number that larger than any distance between two points
  }
}

void initialMark(){
  int i;
  for ( i = 0; i < searchNum ; i++)
  {
    searchList[i]->pathMark = 0; // default value 0
  }
}

void initialSearched(){
  int i;
  for ( i = 0; i < searchNum ; i++)
  {
    searchList[i]->searched = 0; // default value 0
  }
}

void printEpoch( Node ** nodeList , int epoch)
{
  FILE *refFile = fopen("map/Final_Map.map","r");
  char outname[sizeof "map/outfile/Final_Map_1500.out"];
  sprintf(outname, "map/outfile/Final_Map_%01.0d.out", epoch);
  // String outname = sprintf('map/outfile/Final_Map%01.0f.out',epoch);
  // FILE *outFile = fopen("map/outfile/Final_Map.out","w");
  FILE *outFile = fopen(outname,"w");
  int i;
  char *dupline;
  const char delimiters[] = " <>=/";

  Node ** nodePair;
  nodePair= ( Node ** )malloc( 2 * sizeof( Node * ) );

  if ( refFile != NULL )
  {
    char line [ 256 ];
    while ( fgets ( line, sizeof line, refFile ) != NULL )
    {
      if (strncmp(line,"<link",5)==0)
      {
        dupline = strdup (line);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        long idtmp = atol(strsep(&dupline,delimiters));
        for( i = 0 ; i < nodeNum ; i++ )
        {
          if ( nodeList[i]->id == idtmp )
          {
            nodePair[0] = nodeList[i];
            break;
          }
        }
        strsep(&dupline,delimiters);
        idtmp = atol(strsep(&dupline,delimiters));
        for(i=0;i<nodeNum;i++)
        {
          if (nodeList[i]->id==idtmp)
          {
            nodePair[1]=nodeList[i];
            break;
          }
        }
        if (nodePair[0]->pathMark == 1) //make sure node with mark 1 is printed first, so that only links on the shortest path are rendered red.(gnuplot)
        {
          fprintf(outFile, "%f %f %d\n",nodePair[1]->lon,nodePair[1]->lat,nodePair[1]->pathMark);
          fprintf(outFile, "%f %f %d\n",nodePair[0]->lon,nodePair[0]->lat,nodePair[0]->pathMark);
        }
        else
        {
          fprintf(outFile, "%f %f %d\n",nodePair[0]->lon,nodePair[0]->lat,nodePair[0]->pathMark);
          fprintf(outFile, "%f %f %d\n",nodePair[1]->lon,nodePair[1]->lat,nodePair[1]->pathMark);
        }
        fprintf(outFile, "\n");
      }
    }
    // printf("Epoch file generated successfully, please check it in the ./map/outfile folder.\n");
    fclose ( outFile );
  }
  else
  {
    perror ( "map/Final_Map.map" );
    exit(1);
  }

  // //free up the memory
  // for (i = 0; i < nodeNum; i++)
  // {
  //   free(nodeList[i]);
  //   nodeList[i] = NULL;
  // }
  // free(nodeList);
  // nodeList = NULL;
}




// int sortNeighbors(Node * node) //for every neighbor of the given node, revise its Dist and check if it is the end node
// {
//   int i;
//   float tmpDist;
//   for( i = 0 ; i < node->neighborNum ; i++)
//   {
//     tmpDist = node->Dist + node->neighborDist[i];
//     if ( tmpDist < node->neighbor[i]->Dist )
//     {
//       node->neighbor[i]->Dist = tmpDist;
//       node->neighbor[i]->parent = node;
//     }
//     if ( node->neighbor[i] == endNode )
//     {
//       return 1;
//     }
//   }
//   return 0;
// }




// Node * FindOptimalInSearchList() // find the node with the smallest distance in search list
// {
//   int i;
//   Node * Opti = searchList[0];
//   for( i = 0 ; i < searchNum ; i++ )
//   {
//     if (searchList[i]->Dist < Opti->Dist)
//     {
//       Opti = searchList[i];
//     }
//   }
//   return Opti;
// }

// void removeFromSearch(Node * remove)  // remove the optimal node from search list
// {
//   int i;
//   for( i = 0 ; i < searchNum ; i++ )
//   {
//     if (searchList[i] == remove)
//     {
//       for(;i<(searchNum-1);i++)
//       {
//         searchList[i]=searchList[i+1];
//       }
//       break;
//     }
//   }
//   searchList[searchNum-1]=NULL;
//   searchNum--;
//   searchList = realloc(searchList,searchNum*sizeof(Node*));
// }
//
//
//
// void constructPath(Node * end){ //mark the parents of end node all the way to the start node
//   end->pathMark = 1;
//   Node * prev = end->parent;
//   while (prev)
//   {
//     prev->pathMark = 1;
//     prev = prev->parent;
//   }
// }
