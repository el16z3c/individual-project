#include "printFile.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void printFile( Node ** nodeList , int epoch)
{
  FILE *refFile = fopen("map/Final_Map.map","r");
  // char outname[sizeof "map/outfile/Final_Map_1500.out"];
  // sprintf(outname, "map/outfile/Final_Map_%01.0d.out", epoch);
  // String outname = sprintf('map/outfile/Final_Map%01.0f.out',epoch);
  FILE *outFile = fopen("map/outfile/Final_Map.out","w");
  // FILE *outFile = fopen(outname,"w");
  int i;
  char *dupline;
  const char delimiters[] = " <>=/";

  Node ** nodePair;
  nodePair= ( Node ** )malloc( 2 * sizeof( Node * ) );

  if ( refFile != NULL )
  {
    char line [ 256 ];
    while ( fgets ( line, sizeof line, refFile ) != NULL )
    {
      if (strncmp(line,"<link",5)==0)
      {
        dupline = strdup (line);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        long idtmp = atol(strsep(&dupline,delimiters));
        for( i = 0 ; i < nodeNum ; i++ )
        {
          if ( nodeList[i]->id == idtmp )
          {
            nodePair[0] = nodeList[i];
            break;
          }
        }
        strsep(&dupline,delimiters);
        idtmp = atol(strsep(&dupline,delimiters));
        for(i=0;i<nodeNum;i++)
        {
          if (nodeList[i]->id==idtmp)
          {
            nodePair[1]=nodeList[i];
            break;
          }
        }
        if (nodePair[0]->pathMark == 1) //make sure node with mark 1 is printed first, so that only links on the shortest path are rendered red.(gnuplot)
        {
          fprintf(outFile, "%f %f %d\n",nodePair[1]->lon,nodePair[1]->lat,nodePair[1]->pathMark);
          fprintf(outFile, "%f %f %d\n",nodePair[0]->lon,nodePair[0]->lat,nodePair[0]->pathMark);
        }
        else
        {
          fprintf(outFile, "%f %f %d\n",nodePair[0]->lon,nodePair[0]->lat,nodePair[0]->pathMark);
          fprintf(outFile, "%f %f %d\n",nodePair[1]->lon,nodePair[1]->lat,nodePair[1]->pathMark);
        }
        fprintf(outFile, "\n");
      }
    }
    printf("Node file generated successfully, please check it in the ./map folder.\n");
    fclose ( outFile );
  }
  else
  {
    perror ( "map/Final_Map.map" );
    exit(1);
  }

  // //free up the memory
  // for (i = 0; i < nodeNum; i++)
  // {
  //   free(nodeList[i]);
  //   nodeList[i] = NULL;
  // }
  // free(nodeList);
  // nodeList = NULL;
}
