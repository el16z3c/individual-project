typedef struct qnode{
long id;//id of node
float lat;//latitude of node
float lon;//longitude of node
float tmpShortest;//distance from source
float searchIndex;
int neighborNum;//record the number of neighbors
struct qnode ** neighbor;//array of neighbor pointers
float neighborDist [10];//length of link, and assume that every node has no more than 10 neighbors
struct qnode * parent;//the parent node
int pathMark;//if node is on the final path
int searched;
}Node;

int nodeNum; //used to share the number of nodes across all c files
