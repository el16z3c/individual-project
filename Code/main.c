#ifndef STRUCTURE
#define STRUCTURE
#include "nodeStructure.h"
#endif
#include "readNode.h"
#include "readEdge.h"
#include "getPoints.h"
#include "Navigator.h"
// #include "printFile.h"
#include <stdlib.h>

int main()
{
  //initialise an array of node pointers
  Node ** nodeList = malloc(sizeof(Node *));
  //read <node> lines from file, create a nodeList, determine id, lat, lon
  nodeList = readNode(nodeList);
  //read <link> lines from file, determine neighbor, neighborNum, neighborDist
  nodeList = readEdge(nodeList);
  // getPoints determine the start and end nodes
  Node * start;
  start = getPoints(nodeList);
  // DijkstraFinder find the nodes on the shortest path and mark them
  nodeList = Navigator( start , nodeList );
  // printFile write all the nodes to file with marks
  // printFile( nodeList );

  return 0;
}
