#ifndef STRUCTURE
#define STRUCTURE
#include "nodeStructure.h"
#endif
Node ** Navigator( Node * startNode, Node ** nodeList);
void localSearch(Node * node, Node * prev, float tmpDist);
void localSearch_2(Node * node, Node * prev, float tmpDist);
Node * moveOrigin(Node * node);
void formFrontier();
void getFrontier(Node * node);
Node * FindOptimalInFrontList(Node * node);
void initialTmpShortest();
void initialSearchIndex();
void initialMark();
void initialSearched();
void printEpoch( Node ** nodeList , int epoch);
