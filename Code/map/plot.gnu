unset colorbox

set palette model RGB defined ( 0 0.65 0.65 0.65 , 1 0.9 0.2 0.1 )

set terminal png size 2000,1500 background rgb 'black'

file_exists(file) = system("[ -f '".file."' ] && echo '1' || echo '0'") + 0

do for [t=0:1500] {
    infile = sprintf('./outfile/Final_map_%01.0f.out',t)
    
    if ( file_exists(infile) ) {
outfile = sprintf('./img/epoch%01.0f.png',t)
system(sprintf("echo epoch %01.0f image generated",t))
set output outfile
plot [-1.565:-1.542][53.801:53.812] infile u 1:2:3 with lines lc palette notitle,\
'' u 1:2:(0.00008):3 with circles palette notitle}
}