#include "readEdge.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


Node ** readEdge(Node ** nodeList)
{
  FILE * edgeFile = fopen("map/Final_Map.map","r");
  char * dupline; //necessary for strsep()
  const char delimiters[] = " <>=/";

  Node ** nodePair; //temporarily hold the two nodes on that link
  nodePair = (Node **)malloc( 2 * sizeof( Node *) );

  float nodeDist; //temporarily hold the distance between the two nodes
  // float length;
  // float distTotal = 0;
  // int distNum = 0;

  initialNeighbor(nodeList); //initialise the neighbor list and neighbor number

  if ( edgeFile != NULL )
  {
    int i;
    char line [ 256 ];
    while ( fgets ( line, sizeof line, edgeFile ) != NULL )
    {
      if (strncmp(line,"<link",5)==0)
      {
        dupline = strdup (line);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        long idtmp = atol(strsep(&dupline,delimiters));
        for( i = 0 ; i < nodeNum ; i++ )
        {
          if ( nodeList[i]->id == idtmp )
          {
            nodePair[0] = nodeList[i];
            break;
          }
        }
        strsep(&dupline,delimiters);
        idtmp = atol(strsep(&dupline,delimiters));
        for( i = 0 ; i < nodeNum ; i++ )
        {
          if ( nodeList[i]->id == idtmp )
          {
            nodePair[1] = nodeList[i];
            break;
          }
        }
        // strsep(&dupline,delimiters);
        // strsep(&dupline,delimiters);
        // strsep(&dupline,delimiters);
        // length = atol(strsep(&dupline,delimiters));
        nodeDist = sqrt((10000 * (nodePair[0]->lat - nodePair[1]->lat)) * (10000 * (nodePair[0]->lat - nodePair[1]->lat)) + (100000 * (nodePair[0]->lon - nodePair[1]->lon)) * (100000 * (nodePair[0]->lon - nodePair[1]->lon)));

        // distTotal += nodeDist;
        // distNum++;
        //
        // printf("ID1:%ld lat:%f lon:%f\nID2:%ld lat:%f lon:%f\nDist:%f\nLength:%f\nratio:%f\n\n",nodePair[0]->id, nodePair[0]->lat, nodePair[0]->lon,nodePair[1]->id, nodePair[1]->lat, nodePair[1]->lon, nodeDist, length, nodeDist/length);

        nodePair[0]->neighborNum++; //neighbor number increment
        nodePair[1]->neighborNum++;
        nodePair[0]->neighbor = realloc(nodePair[0]->neighbor,nodePair[0]->neighborNum*sizeof(Node*)); //realloc neighbor list
        nodePair[1]->neighbor = realloc(nodePair[1]->neighbor,nodePair[1]->neighborNum*sizeof(Node*));
        nodePair[0]->neighbor[nodePair[0]->neighborNum - 1] = nodePair[1]; //set neighbor
        nodePair[1]->neighbor[nodePair[1]->neighborNum - 1] = nodePair[0];
        nodePair[0]->neighborDist[nodePair[0]->neighborNum - 1]= nodeDist; //set distance
        nodePair[1]->neighborDist[nodePair[1]->neighborNum - 1]= nodeDist;
      }
    }
    // printf("Ave:%f\n",distTotal/distNum );
    fclose ( edgeFile );
    return nodeList;
  }
  else
  {
    perror ( "map/Final_Map.map" );
    exit(1);
    return NULL;
  }
}

void initialNeighbor(Node ** nodeList){
  int i;
  for ( i = 0; i < nodeNum ; i++)
  {
    nodeList[i]->neighbor = (Node **)malloc(sizeof( Node * )); //initialise neighbor list
    nodeList[i]->neighborNum = 0; //initialise neighbor number
  }
}
