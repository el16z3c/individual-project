#include "readNode.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

Node ** readNode(Node ** nodeList)
{
  FILE * nodeFile = fopen("map/Final_Map.map","r");
  char * dupline; //necessary for strsep()
  const char delimiters[] = " <>=/";

  nodeNum = 0; //initialise node number

  if ( nodeFile != NULL )
  {
    char line [ 256 ]; //temporarily hold lines
    while ( fgets ( line, sizeof line, nodeFile ) != NULL )
    {
      if (strncmp(line,"<node",5)==0)
      {
        nodeNum++;
        nodeList = realloc(nodeList,nodeNum*sizeof(Node*));
        nodeList[nodeNum-1]= (Node *)malloc( sizeof( Node ) );
        dupline = strdup (line);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        strsep(&dupline,delimiters);
        nodeList[nodeNum-1]->id = atol(strsep(&dupline,delimiters));
        strsep(&dupline,delimiters);
        nodeList[nodeNum-1]->lat = atof(strsep(&dupline,delimiters));
        strsep(&dupline,delimiters);
        nodeList[nodeNum-1]->lon = atof(strsep(&dupline,delimiters));
      }
    }
    fclose ( nodeFile );
  }
  else
  {
    perror ( "map/Final_Map.map" );
    exit(1);
    return NULL;
  }
  return nodeList;
}
